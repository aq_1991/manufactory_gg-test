using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace manufactory.test.game.architecture.data
{
    public class DataModels
    {
        
    }

    #region General Enums
    public enum BulletType
    {
        SingleFire, MultiFire, BigBoom, BirdFormation
    }
    public enum PowerupType
    { 
        Temporary, Permanent
    }
    public enum PowerupUpgrade
    {
        Immortal, IncreasedFireRate, FireBall, ArcFires, BirdSwarm
    }
    #endregion

    #region Meta Data
    [Serializable]
    public class GameMetaData
    {
        public float PlayerMaxBulletSpeed = 0.25f;
        public float PlayerMaxBulletLifeTime = 1.0f;
        public float NPCMaxBulletSpeed = 0.25f;
        public float NPCMaxBulletLifeTime = 1.0f;
        public int MinAsteriodSpawnDelay = 1000;
        public int MaxAsteriodSpawnDelay = 3000;
        public int MinUFOSpawnDelay = 5000;
        public int MaxUFOSpawnDelay = 10000;
        public float MinScaleToSplitAsteriod = 0.5f;
        public int LimitMaxSpawnedUFO = 2;
        public int CurrentScoreToAddOnUFOKill = 10;
        public int CurrentScoreToAddOnAsteriodKill = 10;
        public int PlayerImmortalTimer = 5;
        public int MinProbablityOfUFOSpawn = 1;
        public int MaxProbablityOfUFOSpawn = 9;
        public int MaxPowerUpSpawnedTime = 5000;
        public float MaxMultiFireSpread = 25.0f;

        public List<int> LevelScores = new List<int>()
        { 
            100, 300, 400, 600, 800, 1000, 5000, 10000, 20000, 50000, 100000, 500000, 100000, 200000, 500000,
            1000000, 5000000, 1000000, 2000000, 5000000, 1000000, 500000000, 10000000, 200000000, 500000000
        };

        public GameMetaData()
        {
        }
    }
    #endregion

    #region Player Related Meta Data
    [Serializable]
    public class PlayerMetaData
    {
        public float SpaceShipInteria = 0.5f;
        public float PlayerMovementSpeed = 5.0f;
        public float PlayerTurnSpeed = 5.0f;
        public float PlayerAngularDrag = 5.0f;
        public List<float> BulletFireRates = new List<float>()
        {
            0.15f, 0.175f, 0.5f
        };
        public BulletType defaultBullet = BulletType.SingleFire;

        public PlayerMetaData()
        {
        }
    }
    #endregion

    #region UFO Related Meta Data
    [Serializable]
    public class UFOMetaData
    {
        public float SpaceShipInteria = 0.5f;
        public float UFOMovementSpeed = 0.85f;
        public float UFORotationSpeed = 1000.0f;
        public float UFOAngularDrag = 5.0f;
        public List<float> BulletFireRates = new List<float>()
        {
            1.25f, 0.85f, 1.0f
        };
        public BulletType defaultBullet = BulletType.SingleFire;
        public float UFOMovementPathUpdateDistance = 2.5f;

        public UFOMetaData()
        {
        }
    }
    #endregion

    #region Asteriods Data
    public class BaseAsteriodsMetaData
    {
        public float AsteriodSpeed = 10.0f;
        public float AsteriodMinMass = 1.0f;
        public float AsteriodMaxMass = 5.0f;
        public float AsteriodAngularDrag = 1.0f;
        public float AsteriodLinearDrag = 1.0f;
        public float MinScale = 1.0f;
        public float MaxScale = 2.0f;
        public float RotationSpeed = 1.25f;
        public float RandomizeAngle = 15.0f;
        public float MaxAsteriodLife = 30.0f;
    }
    [Serializable]
    public class SmallAsteriodsMetaData : BaseAsteriodsMetaData
    {
        public AsteriodsType asteriodsType;
        
        public SmallAsteriodsMetaData()
        {
            asteriodsType = AsteriodsType.Small;
            MinScale = 0.5f;
            MaxScale = 1.0f;
            AsteriodMinMass = 1.0f;
            AsteriodMaxMass = 2.5f;
        }
    }
    [Serializable]
    public class MediumAsteriodsMetaData : BaseAsteriodsMetaData
    {
        public AsteriodsType asteriodsType;

        public MediumAsteriodsMetaData()
        {
            asteriodsType = AsteriodsType.Medium;
            MinScale = 1.0f;
            MaxScale = 1.5f;
            AsteriodMinMass = 2.0f;
            AsteriodMaxMass = 3.5f;
        }
    }
    [Serializable]
    public class LargeAsteriodsMetaData : BaseAsteriodsMetaData
    {
        public AsteriodsType asteriodsType;

        public LargeAsteriodsMetaData()
        {
            asteriodsType = AsteriodsType.Large;
            MinScale = 1.0f;
            MaxScale = 2.5f;
            AsteriodMinMass = 3.0f;
            AsteriodMaxMass = 5.0f;
        }
    }

    public enum AsteriodsType
    { 
        Small, Medium, Large
    }

    #endregion

    #region Powerup Meta Data
    [Serializable]
    public class PowerBase
    {
        public float IncreaseRateBy = 0.35f;
        public float Duration = 0.35f;
        public PowerupType powerupType;
        public PowerupUpgrade powerupUpgrade;
    }
    public class ImmortalPowerUp : PowerBase
    {

        public ImmortalPowerUp()
        {
            IncreaseRateBy = 0.0f;
            Duration = 3.0f;
            powerupType = PowerupType.Temporary;
            powerupUpgrade = PowerupUpgrade.Immortal;
        }
    }
    public class IncreaseFireRatePowerUp : PowerBase
    {
        public IncreaseFireRatePowerUp()
        {
            IncreaseRateBy = 0.05f;
            Duration = Mathf.Infinity;
            powerupType = PowerupType.Permanent;
            powerupUpgrade = PowerupUpgrade.IncreasedFireRate;
        }
    }
    public class FireBallPowerUp : PowerBase
    {
        public FireBallPowerUp()
        {
            IncreaseRateBy = 0.15f;
            Duration = 3.0f;
            powerupType = PowerupType.Temporary;
            powerupUpgrade = PowerupUpgrade.FireBall;
        }
    }
    public class ArcFirePowerUp : PowerBase
    {
        public ArcFirePowerUp()
        {
            IncreaseRateBy = 0.15f;
            Duration = 3.0f;
            powerupType = PowerupType.Temporary;
            powerupUpgrade = PowerupUpgrade.ArcFires;
        }
    }
    public class SwarmingBirdPowerUp : PowerBase
    {
        public SwarmingBirdPowerUp()
        {
            IncreaseRateBy = 0.15f;
            Duration = 3.0f;
            powerupType = PowerupType.Temporary;
            powerupUpgrade = PowerupUpgrade.BirdSwarm;
        }
    }


    #endregion
}