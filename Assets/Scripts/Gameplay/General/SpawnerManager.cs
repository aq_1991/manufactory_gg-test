using manufactory.test.game.architecture.behaviours;
using manufactory.test.game.architecture.data;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

namespace manufactory.test.game.architecture.manager
{
    public class SpawnerManager
    {
        private GameplayManager gameplayManager;
        private PoolingManager poolingManager;
        private GameStateManager gameState;
        private GameMetaData gameMetaData;
        private Vector3 spawningDirection;
        private Quaternion asteriodRotation;
        private int currentSpawnedUFO = 1;

        public SpawnerManager()
        {
            GameplayManager = GameplayManager.Instance;
            PoolingManager = GameplayManager.poolingManager;
            gameMetaData = GameManager.Instance.GameState.GameMetaData;
            GameState = GameManager.Instance.GameState;
            StartAsteriodSpawning();
            StartUFOSpawning();
        }
        public GameplayManager GameplayManager { get => gameplayManager; set => gameplayManager = value; }
        public PoolingManager PoolingManager { get => poolingManager; set => poolingManager = value; }
        public Vector3 SpawningDirection { get => spawningDirection; set => spawningDirection = value; }
        public Quaternion AsteriodRotation { get => asteriodRotation; set => asteriodRotation = value; }
        public GameStateManager GameState { get => gameState; set => gameState = value; }
        public int CurrentSpawnedUFO { get => currentSpawnedUFO; set => currentSpawnedUFO = value; }


        #region Asteriod Spawning

        public async void StartAsteriodSpawning()
        {
            await Task.Delay(Random.Range(gameMetaData.MinAsteriodSpawnDelay, gameMetaData.MaxAsteriodSpawnDelay));
            Transform temp = GameplayManager.ObjectSpawnLocations[Random.Range(0, GameplayManager.ObjectSpawnLocations.Length)];
            
            if (temp == null)
                return;

            SpawningDirection = temp.position;
            AsteriodBehaviour asteriodBehaviour = poolingManager.ReturnAsteroidsReferences((AsteriodsType)Random.Range((int)AsteriodsType.Small, (int)AsteriodsType.Large + 1));
            switch (asteriodBehaviour.asteriodsType)
            {
                case AsteriodsType.Small:
                    AsteriodRotation = Quaternion.AngleAxis(RandomizedAngleVariation(asteriodBehaviour.SmallAsteriod.RandomizeAngle), Vector3.forward);
                    asteriodBehaviour.transform.localScale = ReturnVectorScale(Random.Range(asteriodBehaviour.SmallAsteriod.MinScale, asteriodBehaviour.SmallAsteriod.MaxScale));
                    break;
                case AsteriodsType.Medium:
                    AsteriodRotation = Quaternion.AngleAxis(RandomizedAngleVariation(asteriodBehaviour.MediumAsteriod.RandomizeAngle), Vector3.forward);
                    asteriodBehaviour.transform.localScale = ReturnVectorScale(Random.Range(asteriodBehaviour.MediumAsteriod.MinScale, asteriodBehaviour.MediumAsteriod.MaxScale));
                    break;
                case AsteriodsType.Large:
                    AsteriodRotation = Quaternion.AngleAxis(RandomizedAngleVariation(asteriodBehaviour.LargeAsteriod.RandomizeAngle), Vector3.forward);
                    asteriodBehaviour.transform.localScale = ReturnVectorScale(Random.Range(asteriodBehaviour.LargeAsteriod.MinScale, asteriodBehaviour.LargeAsteriod.MaxScale));
                    break;
            }
            asteriodBehaviour.transform.SetPositionAndRotation(SpawningDirection, AsteriodRotation);
            asteriodBehaviour.gameObject.SetActive(true);
            asteriodBehaviour.MovementDirection(AsteriodRotation * -SpawningDirection);
            StartAsteriodSpawning();
        }
        public void SpawnSplitAsteriods(float value, Transform currentAsteriodLocation)
        {
            if (value >= GameState.SmallAsteriodsMetaData.MinScale && value <= GameState.SmallAsteriodsMetaData.MaxScale)
            {
                SpawnAsteriod(currentAsteriodLocation, AsteriodsType.Small);
            }
            else if (value >= GameState.MediumAsteriodsMetaData.MinScale && value <= GameState.MediumAsteriodsMetaData.MaxScale)
            {
                SpawnAsteriod(currentAsteriodLocation, AsteriodsType.Medium);
            }
        }
        public void SpawnSplitAsteriods(AsteriodsType asteriodsType, Transform currentAsteriodLocation)
        {
            SpawnAsteriod(currentAsteriodLocation, asteriodsType);
        }
        private void SpawnAsteriod(Transform currentAsteriodLocation, AsteriodsType asteriodsType)
        {
            SpawningDirection = currentAsteriodLocation.position;
            AsteriodBehaviour asteriodBehaviour = poolingManager.ReturnAsteroidsReferences(asteriodsType);
            asteriodBehaviour.transform.SetPositionAndRotation(SpawningDirection, currentAsteriodLocation.localRotation);
            switch (asteriodsType)
            {
                case AsteriodsType.Small:
                    asteriodBehaviour.transform.localScale = ReturnVectorScale(Random.Range(asteriodBehaviour.SmallAsteriod.MinScale, asteriodBehaviour.SmallAsteriod.MaxScale));
                    break;
                case AsteriodsType.Medium:
                    asteriodBehaviour.transform.localScale = ReturnVectorScale(Random.Range(asteriodBehaviour.MediumAsteriod.MinScale, asteriodBehaviour.MediumAsteriod.MaxScale));
                    break;
                case AsteriodsType.Large:
                    asteriodBehaviour.transform.localScale = ReturnVectorScale(Random.Range(asteriodBehaviour.LargeAsteriod.MinScale, asteriodBehaviour.LargeAsteriod.MaxScale));
                    break;
            }
            asteriodBehaviour.gameObject.SetActive(true);
            asteriodBehaviour.MovementDirection(Random.insideUnitCircle.normalized * SpawningDirection);
        }
        private Vector3 ReturnVectorScale(float randomScale)
        {
            return new Vector3(randomScale, randomScale, randomScale);
        }
        private float RandomizedAngleVariation(float value)
        {
            return Random.Range(-value, value);
        }


        #endregion

        #region UFO Spawning

        public async void StartUFOSpawning()
        {
            await Task.Delay(100);
            if (Random.Range(GameState.GameMetaData.MinProbablityOfUFOSpawn, GameState.GameMetaData.MaxProbablityOfUFOSpawn) % 2 == 0 && CurrentSpawnedUFO <= GameState.GameMetaData.LimitMaxSpawnedUFO)
            {
                CurrentSpawnedUFO++;
                await Task.Delay(Random.Range(gameMetaData.MinUFOSpawnDelay, gameMetaData.MaxUFOSpawnDelay));
                Transform temp = GameplayManager.ObjectSpawnLocations[Random.Range(0, GameplayManager.ObjectSpawnLocations.Length)];

                if (!temp)
                    return;

                SpawningDirection = temp.position;
                EnemyBehaviour enemyBehaviour = poolingManager.ReturnUFO();
                enemyBehaviour.transform.SetPositionAndRotation(SpawningDirection, temp.localRotation);
                enemyBehaviour.gameObject.SetActive(true);
            }
            StartUFOSpawning();
        }


        #endregion

    }
}