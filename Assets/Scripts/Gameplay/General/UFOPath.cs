using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace manufactory.test.gameplay
{
    public class UFOPath : MonoBehaviour
    {
        public Transform[] ufoPathWay;
        public static UFOPath Instance;
        private void OnEnable()
        {
            if (Instance == null)
            {
                Instance = this;
            }
        }
        public Transform ReturnRandomPathLocation()
        {
            return ufoPathWay[Random.Range(0, ufoPathWay.Length)];
        }
        private void OnDisable()
        {
            Instance = null;
        }
    }
}