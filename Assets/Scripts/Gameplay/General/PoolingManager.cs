using manufactory.test.game.architecture.behaviours;
using manufactory.test.game.architecture.data;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace manufactory.test.game.architecture.manager
{
    public class PoolingManager : MonoBehaviour
    {
        [Header("------ Player Related Objects --------")]
        public List<GameObject> PlayerSingleBullets = new List<GameObject>();
        public List<GameObject> PlayerMultiBullets = new List<GameObject>();
        public List<GameObject> PlayerBoomBullets = new List<GameObject>();
        public List<GameObject> PlayerSwarmBullets = new List<GameObject>();

        [Header("------- NPC Related Objects --------")]
        public List<GameObject> NPCSingleBullets = new List<GameObject>();
        public List<GameObject> NPC_UFO = new List<GameObject>();
        public List<GameObject> RandomPowerUp = new List<GameObject>();

        [Header("------ Asteroids Related Objects -------")]
        public List<GameObject> SmallAsteroids = new List<GameObject> ();
        public List<GameObject> MediumAsteroids = new List<GameObject>();
        public List<GameObject> LargeAsteroids = new List<GameObject> ();

        #region Remove and Add Player Bullets
        
        public BulletBehaviour ReturnPlayerBullet(BulletType bulletType)
        {
            BulletBehaviour temp = null;
            switch (bulletType)
            {
                case BulletType.SingleFire:
                    temp = PlayerSingleBullets[0].GetComponent<BulletBehaviour>();
                    PlayerSingleBullets.Remove(temp.gameObject);
                    break;
                case BulletType.MultiFire:
                    temp = PlayerMultiBullets[0].GetComponent<BulletBehaviour>();
                    PlayerMultiBullets.Remove(temp.gameObject);
                    break;
                case BulletType.BigBoom:
                    temp = PlayerBoomBullets[0].GetComponent<BulletBehaviour>();
                    PlayerBoomBullets.Remove(temp.gameObject);
                    break;
                case BulletType.BirdFormation:
                    temp = PlayerSwarmBullets[0].GetComponent<BulletBehaviour>();
                    PlayerSwarmBullets.Remove(temp.gameObject);
                    break;
            }
            return temp;
        }
        public void AddBullet(BulletType bulletType, GameObject temp)
        {
            if (!temp)
                return;

            switch (bulletType)
            {
                case BulletType.SingleFire:
                    PlayerSingleBullets.Add(temp);
                    break;
                case BulletType.MultiFire:
                    PlayerMultiBullets.Add(temp);
                    break;
                case BulletType.BigBoom:
                    PlayerBoomBullets.Add(temp);
                    break;
                case BulletType.BirdFormation:
                    PlayerSwarmBullets.Add(temp);
                    break;
            }
        }

        #endregion

        #region Remove and Add Asteriods

        public AsteriodBehaviour ReturnAsteroidsReferences(AsteriodsType asteriodsType)
        {
            GameObject temp = null;
            switch (asteriodsType)
            {
                case AsteriodsType.Small:
                    temp = SmallAsteroids[0];
                    SmallAsteroids.Remove(temp);
                    break;
                case AsteriodsType.Medium:
                    temp = MediumAsteroids[0];
                    MediumAsteroids.Remove(temp);
                    break;
                case AsteriodsType.Large:
                    temp = LargeAsteroids[0];
                    LargeAsteroids.Remove(temp);
                    break;
            }
            temp.GetComponent<AsteriodBehaviour>().InitializeAsteriod();
            return temp.GetComponent<AsteriodBehaviour>();
        }

        public void AddAsteroids(AsteriodsType asteriodsType, GameObject temp)
        {
            if (!temp)
                return;

            switch (asteriodsType)
            {
                case AsteriodsType.Small:
                    SmallAsteroids.Add(temp);
                    break;
                case AsteriodsType.Medium:
                    MediumAsteroids.Add(temp);
                    break;
                case AsteriodsType.Large:
                    LargeAsteroids.Add(temp);
                    break;
            }
        }

        #endregion

        #region Remove and Add NPC Bullets
        public BulletBehaviour ReturnNPCBullet()
        {
            BulletBehaviour temp = NPCSingleBullets[0].GetComponent<BulletBehaviour>();
            NPCSingleBullets.Remove(temp.gameObject);
            return temp;
        }
        public void AddNPCBullet(GameObject temp)
        {
            if (!temp)
                return;

            NPCSingleBullets.Add(temp);
        }

        #endregion

        #region Remove and Add UFO's
        public EnemyBehaviour ReturnUFO()
        {
            EnemyBehaviour temp = NPC_UFO[0].GetComponent<EnemyBehaviour>();
            NPC_UFO.Remove(temp.gameObject);
            return temp;
        }
        public void AddUFO(GameObject temp)
        {
            if (!temp)
                return;

            NPC_UFO.Add(temp);
        }
        #endregion

        #region Remove and Add Power ups

        public PlayerPowerupBehaviour ReturnPowerUpForPlayer()
        {
            PlayerPowerupBehaviour playerPowerupBehaviour = RandomPowerUp[0].GetComponent<PlayerPowerupBehaviour>();
            RandomPowerUp.Remove(playerPowerupBehaviour.gameObject);
            PowerupUpgrade upgrade = (PowerupUpgrade)Random.Range(0, 5);
            playerPowerupBehaviour.InitPowerUp(upgrade);
            playerPowerupBehaviour.gameObject.SetActive(true);
            return playerPowerupBehaviour;
        }

        public void AddPowerUp(GameObject temp)
        {
            if (!temp)
                return;

            RandomPowerUp.Add(temp);
        }

        #endregion

    }
}
