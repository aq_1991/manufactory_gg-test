using manufactory.test.game.architecture.data;
using manufactory.test.game.architecture.manager;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace manufactory.test.game.architecture.behaviours
{
    public class AsteriodBehaviour : AsteriodBase
    {
        private Rigidbody2D rb;
        private float currentSize = 1.0f;
        private float currentRotationSpeed = 1.0f;
        public GameStateManager gameState;
        public AsteriodsType asteriodsType;
        private SmallAsteriodsMetaData smallAsteriod = null;
        private MediumAsteriodsMetaData mediumAsteriod = null;
        private LargeAsteriodsMetaData largeAsteriod = null;
        private PoolingManager poolingManager;
        private bool isDisabled;
        private float maxBulletLife;
        private GameplayManager gameplayManager;

        public float CurrentSize { get => currentSize; set => currentSize = value; }
        public SmallAsteriodsMetaData SmallAsteriod { get => smallAsteriod; set => smallAsteriod = value; }
        public MediumAsteriodsMetaData MediumAsteriod { get => mediumAsteriod; set => mediumAsteriod = value; }
        public LargeAsteriodsMetaData LargeAsteriod { get => largeAsteriod; set => largeAsteriod = value; }
        public PoolingManager PoolingManager { get => poolingManager; set => poolingManager = value; }
        public float MaxBulletLife { get => maxBulletLife; set => maxBulletLife = value; }
        public GameplayManager GameplayManager { get => gameplayManager; set => gameplayManager = value; }

        public override void InitializeAsteriod()
        {
            isDisabled = false;
            ApplyAsteriodSettings();
        }

        #region Asteriods Settings Updated

        private void ApplyAsteriodSettings()
        {
            rb = GetComponent<Rigidbody2D>();
            gameState = GameManager.Instance.GameState;
            poolingManager = GameplayManager.Instance.poolingManager;
            GameplayManager = GameplayManager.Instance;

            switch (asteriodsType)
            {
                case AsteriodsType.Small:
                    SmallAsteriodSettings();
                    break;
                case AsteriodsType.Medium:
                    MediumAsteriodSettings();
                    break;
                case AsteriodsType.Large:
                    LargeAsteriodSettings();
                    break;
            }
            transform.localScale = Vector3.one * CurrentSize;
        }
        private void SmallAsteriodSettings()
        {
            SmallAsteriod = gameState.SmallAsteriodsMetaData;
            CurrentSize = Mathf.Clamp(Random.Range(SmallAsteriod.MinScale, SmallAsteriod.MaxScale), SmallAsteriod.MinScale, SmallAsteriod.MaxScale);
            rb.mass = Random.Range(SmallAsteriod.AsteriodMinMass, SmallAsteriod.AsteriodMaxMass);
            currentRotationSpeed = SmallAsteriod.RotationSpeed;
            MaxBulletLife = SmallAsteriod.MaxAsteriodLife;
        }
        private void MediumAsteriodSettings()
        {
            MediumAsteriod = gameState.MediumAsteriodsMetaData;
            CurrentSize = Mathf.Clamp(Random.Range(MediumAsteriod.MinScale, MediumAsteriod.MaxScale), MediumAsteriod.MinScale, MediumAsteriod.MaxScale);
            rb.mass = Random.Range(MediumAsteriod.AsteriodMinMass, MediumAsteriod.AsteriodMaxMass);
            currentRotationSpeed = MediumAsteriod.RotationSpeed;
            MaxBulletLife = MediumAsteriod.MaxAsteriodLife;
        }
        private void LargeAsteriodSettings()
        {
            LargeAsteriod = gameState.LargeAsteriodsMetaData;
            CurrentSize = Mathf.Clamp(Random.Range(LargeAsteriod.MinScale, LargeAsteriod.MaxScale), LargeAsteriod.MinScale, LargeAsteriod.MaxScale);
            rb.mass = Random.Range(LargeAsteriod.AsteriodMinMass, LargeAsteriod.AsteriodMaxMass);
            currentRotationSpeed = LargeAsteriod.RotationSpeed;
            MaxBulletLife = LargeAsteriod.MaxAsteriodLife;
        }

        #endregion

        private void FixedUpdate()
        {
            if (rb == null)
                return;

            rb.AddTorque(Time.fixedDeltaTime * currentRotationSpeed);
        }

        public override void MovementDirection(Vector2 vector2)
        {
            switch (asteriodsType)
            {
                case AsteriodsType.Small:
                    rb.AddForce(vector2 * SmallAsteriod.AsteriodSpeed);
                    break;
                case AsteriodsType.Medium:
                    rb.AddForce(vector2 * MediumAsteriod.AsteriodSpeed);
                    break;
                case AsteriodsType.Large:
                    rb.AddForce(vector2 * LargeAsteriod.AsteriodSpeed);
                    break;
            }

            StartCoroutine(DisableAsteriod());
        }
        private void OnCollisionEnter2D(Collision2D collision)
        {
            OnHit(collision);
        }

        public override void OnHit(Collision2D collision)
        {
            if (collision.transform.GetComponent<BulletBehaviour>())
            {
                // 2 ways to break of the asteroid 1 is using scales and 1 using specifically downgrading it using enums

                //if (CurrentSize / 2 >= gameState.GameMetaData.MinScaleToSplitAsteriod && asteriodsType != AsteriodsType.Small)
                //{
                //    GameplayManager.asteriodSpawner.SpawnSplitAsteriods(CurrentSize / 2f, transform);
                //    GameplayManager.asteriodSpawner.SpawnSplitAsteriods(CurrentSize / 2f, transform);
                //}

                switch (asteriodsType)
                {
                    case AsteriodsType.Small:
                        //small is already small no need to spawn again
                        GameplayManager.UpdateScores(gameState.GameMetaData.CurrentScoreToAddOnAsteriodKill);
                        break;
                    case AsteriodsType.Medium:
                        GameplayManager.spawnerManager.SpawnSplitAsteriods(AsteriodsType.Small, transform);
                        GameplayManager.spawnerManager.SpawnSplitAsteriods(AsteriodsType.Small, transform);
                        break;
                    case AsteriodsType.Large:
                        GameplayManager.spawnerManager.SpawnSplitAsteriods(AsteriodsType.Medium, transform);
                        GameplayManager.spawnerManager.SpawnSplitAsteriods(AsteriodsType.Medium, transform);
                        break;
                }
            }
            gameObject.SetActive(false);
        }
        private IEnumerator DisableAsteriod()
        {
            yield return new WaitForSecondsRealtime(MaxBulletLife);
            if (!isDisabled)
                gameObject.SetActive(false);
        }
        private void OnDisable()
        {
            isDisabled = true;
            poolingManager.AddAsteroids(asteriodsType, gameObject);
        }
    }
}
