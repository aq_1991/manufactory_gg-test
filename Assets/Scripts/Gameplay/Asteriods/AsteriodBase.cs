using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace manufactory.test.game.architecture
{
    public abstract class AsteriodBase : MonoBehaviour
    {
        public abstract void InitializeAsteriod();
        public abstract void MovementDirection(Vector2 vector2);
        public abstract void OnHit(Collision2D collision);
    }
}
