using manufactory.test.game.architecture.data;
using manufactory.test.game.architecture.manager;
using System.Collections;
using System.Threading.Tasks;
using UnityEngine;

namespace manufactory.test.game.architecture.behaviours
{
    public class PlayerBehaviour : PlayerBase
    {
        private Rigidbody2D rb;
        private float playerMovementSpeed;
        private float playerTurnSpeed;
        private float fireRate;
        private float nextBulletTime;
        public Transform bulletSpawnPoint;
        public GameObject ImmortalShield;
        [SerializeField]
        private BulletType currentSelectedBulletType;
        private GameStateManager gameState;
        private PoolingManager poolingManager;
        private bool isFiring;
        private bool isDead;
        private bool isImmortal;
        private bool isCollidingWithBoundries;

        public float PlayerMovementSpeed { get => playerMovementSpeed; set => playerMovementSpeed = value; }
        public float PlayerTurnSpeed { get => playerTurnSpeed; set => playerTurnSpeed = value; }
        public BulletType CurrentSelectedBulletType 
        { 
            get => currentSelectedBulletType;
            set
            {
                currentSelectedBulletType = value;
                switch (currentSelectedBulletType)
                {
                    case BulletType.SingleFire:
                        FireRate = gameState.PlayerMetaData.BulletFireRates[0];
                        break;
                    case BulletType.MultiFire:
                        FireRate = gameState.PlayerMetaData.BulletFireRates[1];
                        break;
                    case BulletType.BigBoom:
                        FireRate = gameState.PlayerMetaData.BulletFireRates[2];
                        break;
                }
            }
        }
        public float FireRate { get => fireRate; set => fireRate = value; }
        public bool IsFiring { get => isFiring; set => isFiring = value; }
        public bool IsDead { get => isDead; set => isDead = value; }
        public bool IsImmortal { get => isImmortal; set => isImmortal = value; }

        private void Start()
        {
            InitializePlayer();
        }
        private void OnEnable()
        {
            IsDead = false;
        }
        private void FixedUpdate()
        {
            if (rb == null || IsDead)
                return;

            Movement();
            Fire(Input.GetAxis("Fire1") > 0 && Time.time > nextBulletTime, CurrentSelectedBulletType);
        }
        public override void InitializePlayer()
        {
            rb = GetComponent<Rigidbody2D>();
            gameState = GameManager.Instance.GameState;
            GameManager.Instance.PlayerBehaviour = this;
            rb.drag = gameState.PlayerMetaData.SpaceShipInteria;
            PlayerMovementSpeed = gameState.PlayerMetaData.PlayerMovementSpeed;
            PlayerTurnSpeed = gameState.PlayerMetaData.PlayerTurnSpeed;
            rb.angularDrag = gameState.PlayerMetaData.PlayerAngularDrag;
            CurrentSelectedBulletType = gameState.PlayerMetaData.defaultBullet;
            poolingManager = GameplayManager.Instance.poolingManager;
        }
        public override void Fire(bool isFiring, BulletType bulletType)
        {
            IsFiring = isFiring;
            if (isFiring)
            {
                nextBulletTime = Time.time + FireRate;

                if (bulletType == BulletType.MultiFire)
                {
                    #region Multi Bullet Spawns

                    AngularBulletsSpawn(0);
                    AngularBulletsSpawn(gameState.GameMetaData.MaxMultiFireSpread);
                    AngularBulletsSpawn(-gameState.GameMetaData.MaxMultiFireSpread);

                    #endregion
                }
                else
                {
                    poolingManager.ReturnPlayerBullet(bulletType).BulletFired(Vector2.up, bulletSpawnPoint.position, transform.rotation, false);
                }
            }
        }
        private void AngularBulletsSpawn(float spawningRotation)
        {
            BulletBehaviour bulletBehaviour = poolingManager.ReturnPlayerBullet(BulletType.MultiFire);

            bulletBehaviour.BulletFired(Vector2.up, bulletSpawnPoint.position,
                Quaternion.RotateTowards(transform.rotation,
                Quaternion.Euler(transform.rotation.x, transform.rotation.y, transform.rotation.z + spawningRotation), spawningRotation),
                false);
        }
        public override void Movement()
        {
            rb.AddForce(Input.GetAxis("Vertical") * PlayerMovementSpeed * transform.up);

            rb.AddTorque(Input.GetAxis("Horizontal") * PlayerTurnSpeed);
        }

        public override void UpdateBulletType(BulletType bulletType)
        {
            CurrentSelectedBulletType = bulletType;
        }

        public override void OnHit(Collision2D collision2D)
        {
            if (IsImmortal)
                return;

            if (collision2D.transform.GetComponent<BulletBehaviour>() || collision2D.transform.GetComponent<EnemyBehaviour>() || collision2D.transform.GetComponent<AsteriodBehaviour>())
            {
                IsFiring = false;
                IsDead = true;
                GameplayManager.Instance.UpdateLives();
                GameplayManager.Instance.RespawnPlayer();
                gameObject.SetActive(false);
            }
        }
        private void OnCollisionEnter2D(Collision2D collision)
        {
            OnHit(collision);
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.transform.CompareTag("ScreenBoundries") && !isCollidingWithBoundries)
            {
                isCollidingWithBoundries = true;
                transform.position = new Vector3((-1 * transform.position.x), (-1 * transform.position.y), transform.position.z);
            }
        }
        private void OnTriggerStay2D(Collider2D collision)
        {
            if (collision.transform.CompareTag("ScreenBoundries"))
            {
                isCollidingWithBoundries = true;
            }
        }
        private void OnTriggerExit2D(Collider2D collision)
        {
            if (collision.transform.CompareTag("ScreenBoundries"))
            {
                isCollidingWithBoundries = false;
            }
        }
        public IEnumerator StartImmortalTimer()
        {
            IsImmortal = true;
            ImmortalShield.SetActive(IsImmortal);
            yield return new WaitForSecondsRealtime(gameState.GameMetaData.PlayerImmortalTimer);
            IsImmortal = false;
            ImmortalShield.SetActive(IsImmortal);
        }
        public IEnumerator StartImmortalTimer(int timer)
        {
            IsImmortal = true;
            ImmortalShield.SetActive(IsImmortal);
            yield return new WaitForSecondsRealtime(timer);
            IsImmortal = false;
            ImmortalShield.SetActive(IsImmortal);
        }

        public override void UpdateBulletFireRate(PowerupUpgrade powerupUpgrade, float upgradeFireRateBy)
        {
            switch (powerupUpgrade)
            {
                case PowerupUpgrade.Immortal:
                    break;
                case PowerupUpgrade.IncreasedFireRate:
                    FireRate -= upgradeFireRateBy;
                    break;
                case PowerupUpgrade.FireBall:
                    FireRate = upgradeFireRateBy;
                    break;
                case PowerupUpgrade.ArcFires:
                    FireRate = upgradeFireRateBy;
                    break;
                case PowerupUpgrade.BirdSwarm:
                    FireRate = upgradeFireRateBy;
                    break;
            }
        }

        public override void UpdateBulletType(BulletType bulletType, float updateFireRateBy, int duration)
        {
            StartCoroutine(LimitedFirePowerUp(bulletType, updateFireRateBy, duration));
        }
        private IEnumerator LimitedFirePowerUp(BulletType bulletType, float updateFireRateBy, int duration)
        {
            float tempFireRate = FireRate;
            fireRate = updateFireRateBy;
            UpdateBulletType(bulletType);
            yield return new WaitForSecondsRealtime(duration);
            fireRate = tempFireRate;
            UpdateBulletType(BulletType.SingleFire);
        }
    }
}

