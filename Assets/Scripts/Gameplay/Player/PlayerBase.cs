using manufactory.test.game.architecture.data;
using UnityEngine;

namespace manufactory.test.game.architecture
{
    public abstract class PlayerBase : MonoBehaviour
    {
        public abstract void InitializePlayer();
        public abstract void Movement();
        public abstract void UpdateBulletType(BulletType bulletType);
        public abstract void UpdateBulletType(BulletType bulletType, float updateFireRateBy, int duration);
        public abstract void UpdateBulletFireRate(PowerupUpgrade powerupUpgrade, float updateFireRateBy);
        public abstract void Fire(bool isFiring, BulletType bulletType);
        public abstract void OnHit(Collision2D collision2D);
    }
}
