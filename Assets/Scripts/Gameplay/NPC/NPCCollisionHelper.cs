using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace manufactory.test.game.architecture.behaviours
{
    public class NPCCollisionHelper : MonoBehaviour
    {
        public EnemyBehaviour enemyBehaviour;

        private void OnCollisionEnter2D(Collision2D collision)
        {
            enemyBehaviour.OnHit(collision);
        }
    }
}
