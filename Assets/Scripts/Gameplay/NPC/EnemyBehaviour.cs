using manufactory.test.game.architecture.data;
using manufactory.test.game.architecture.manager;
using manufactory.test.gameplay;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace manufactory.test.game.architecture.behaviours
{
    public class EnemyBehaviour : PlayerBase
    {
        [SerializeField]
        private Rigidbody2D rb;
        private float movementSpeed;
        private float rotationSpeed;
        private float uFOMovementPathUpdateDistance;

        private Transform currentMovementLocation;
        private PoolingManager poolingManager;
        private GameStateManager gameState;
        private NPCCollisionHelper collisionHelper;
        private UFOPath uFOPath;
        [SerializeField]
        private BulletType currentSelectedBulletType;
        private float fireRate;
        private float nextBulletTime;
        public Transform bulletSpawnPoint;

        public float MovementSpeed { get => movementSpeed; set => movementSpeed = value; }
        public float RotationSpeed { get => rotationSpeed; set => rotationSpeed = value; }
        public float UFOMovementPathUpdateDistance { get => uFOMovementPathUpdateDistance; set => uFOMovementPathUpdateDistance = value; }
        public BulletType CurrentSelectedBulletType 
        { 
            get => currentSelectedBulletType;
            set
            {
                currentSelectedBulletType = value;
                switch (currentSelectedBulletType)
                {
                    case BulletType.SingleFire:
                        FireRate = gameState.UFOMetaData.BulletFireRates[0];
                        break;
                    case BulletType.MultiFire:
                        FireRate = gameState.UFOMetaData.BulletFireRates[1];
                        break;
                    case BulletType.BigBoom:
                        FireRate = gameState.UFOMetaData.BulletFireRates[2];
                        break;
                }
            }
        }

        public float FireRate { get => fireRate; set => fireRate = value; }

        private void Start()
        {
            InitializePlayer();
        }
        private void FixedUpdate()
        {
            Movement();
            Fire(Time.time > nextBulletTime, currentSelectedBulletType);
        }
        public override void Fire(bool isFiring, BulletType bulletType)
        {
            LookAtPlayer();
            if (isFiring)
            {
                nextBulletTime = Time.time + FireRate;
                poolingManager.ReturnNPCBullet().BulletFired(Vector2.down, bulletSpawnPoint.position, transform.rotation, true);
            }
        }
        private void LookAtPlayer()
        {
            var offset = 90f;
            Vector2 direction = GameManager.Instance.PlayerBehaviour.transform.position - transform.position;
            direction.Normalize();
            float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
            transform.rotation = Quaternion.Euler(Vector3.forward * (angle + offset));
        }

        public override void InitializePlayer()
        {
            gameState = GameManager.Instance.GameState;
            MovementSpeed = gameState.UFOMetaData.UFOMovementSpeed;
            RotationSpeed = gameState.UFOMetaData.UFORotationSpeed;
            UFOMovementPathUpdateDistance = gameState.UFOMetaData.UFOMovementPathUpdateDistance;
            CurrentSelectedBulletType = gameState.UFOMetaData.defaultBullet;
            uFOPath = UFOPath.Instance;
            currentMovementLocation = uFOPath.ReturnRandomPathLocation();
            poolingManager = GameplayManager.Instance.poolingManager;
        }

        public override void Movement()
        {
            if (uFOPath && Vector2.Distance(transform.position, currentMovementLocation.position) < UFOMovementPathUpdateDistance)
            {
                currentMovementLocation = uFOPath.ReturnRandomPathLocation();
            }
            rb.AddTorque(Time.fixedDeltaTime * RotationSpeed);
            transform.position = Vector2.Lerp(transform.position, currentMovementLocation.position, Time.fixedDeltaTime * MovementSpeed);
        }

        public override void UpdateBulletType(BulletType bulletType)
        {
            throw new System.NotImplementedException();
        }

        public override void OnHit(Collision2D collision2D)
        {
            poolingManager.ReturnPowerUpForPlayer().transform.SetPositionAndRotation(transform.position, Quaternion.identity);
            gameObject.SetActive(false);
        }
        private void OnDisable()
        {
            if(GameplayManager.Instance)
                GameplayManager.Instance.spawnerManager.CurrentSpawnedUFO--;

            poolingManager.AddUFO(gameObject);
        }

        public override void UpdateBulletType(BulletType bulletType, float updateFireRateBy, int duration)
        {
            throw new System.NotImplementedException();
        }

        public override void UpdateBulletFireRate(PowerupUpgrade powerupUpgrade, float updateFireRateBy)
        {
            throw new System.NotImplementedException();
        }
    }
}