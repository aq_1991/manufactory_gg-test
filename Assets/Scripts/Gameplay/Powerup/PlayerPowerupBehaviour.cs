using manufactory.test.game.architecture.data;
using manufactory.test.game.architecture.manager;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

namespace manufactory.test.game.architecture.behaviours
{
    public class PlayerPowerupBehaviour : PowerUpBase
    {
        private GameStateManager gameState;
        private PoolingManager poolingManager;
        public override void InitPowerUp(PowerupUpgrade powerupUpgrade)
        {
            gameState = GameplayManager.Instance.gameState;
            poolingManager = GameplayManager.Instance.poolingManager;
            switch (powerupUpgrade)
            {
                case PowerupUpgrade.Immortal:
                    powerBase = gameState.ImmortalPower;
                    break;
                case PowerupUpgrade.IncreasedFireRate:
                    powerBase = gameState.IncreaseFire;
                    break;
                case PowerupUpgrade.FireBall:
                    powerBase = gameState.FireBall;
                    break;
                case PowerupUpgrade.ArcFires:
                    powerBase = gameState.ArcFire;
                    break;
                case PowerupUpgrade.BirdSwarm:
                    powerBase = gameState.SwarmingBird;
                    break;
            }

            StartCountDownTimer();
        }
        private async void StartCountDownTimer()
        {
            await Task.Delay(gameState.GameMetaData.MaxPowerUpSpawnedTime);
            gameObject.SetActive(false);
        }
        public override void OnPickUp(PlayerBehaviour playerBehaviour)
        {
            switch (powerBase.powerupUpgrade)
            {
                case PowerupUpgrade.Immortal:
                    StartCoroutine(playerBehaviour.StartImmortalTimer(Convert.ToInt32(powerBase.Duration)));
                    break;
                case PowerupUpgrade.IncreasedFireRate:
                    playerBehaviour.UpdateBulletFireRate(powerBase.powerupUpgrade, powerBase.IncreaseRateBy);
                    break;
                case PowerupUpgrade.FireBall:
                    playerBehaviour.UpdateBulletType(BulletType.BigBoom, powerBase.IncreaseRateBy, Convert.ToInt32(powerBase.Duration));
                    break;
                case PowerupUpgrade.ArcFires:
                    playerBehaviour.UpdateBulletType(BulletType.MultiFire, powerBase.IncreaseRateBy, Convert.ToInt32(powerBase.Duration));
                    break;
                case PowerupUpgrade.BirdSwarm:
                    playerBehaviour.UpdateBulletType(BulletType.BirdFormation, powerBase.IncreaseRateBy, Convert.ToInt32(powerBase.Duration));
                    break;
            }
            //
            gameObject.SetActive(false);
        }
        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.transform.GetComponent<PlayerBehaviour>())
            {
                OnPickUp(collision.transform.GetComponent<PlayerBehaviour>());
            }
        }
        private void OnDisable()
        {
            poolingManager.AddPowerUp(gameObject);
        }
    }
}
