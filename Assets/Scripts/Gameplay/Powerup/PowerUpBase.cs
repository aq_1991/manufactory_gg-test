using manufactory.test.game.architecture.behaviours;
using manufactory.test.game.architecture.data;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace manufactory.test.game.architecture
{
    public abstract class PowerUpBase : MonoBehaviour
    {
        public PowerBase powerBase;
        public abstract void InitPowerUp(PowerupUpgrade powerupUpgrade);
        public abstract void OnPickUp(PlayerBehaviour playerBehaviour);
    }
}
