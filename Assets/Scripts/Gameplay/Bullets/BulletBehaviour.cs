using manufactory.test.game.architecture.data;
using manufactory.test.game.architecture.manager;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

namespace manufactory.test.game.architecture.behaviours
{
    public class BulletBehaviour : BulletBase
    {
        private Rigidbody2D rb;
        private bool hasFired;
        private Vector2 movmentDirection;
        private float bulletMovementSpeed;
        private float maxBulletLife = 10.0f;
        public BulletType currentBulletType;
        private bool isDisabled;
        private bool isNPCBullet;
        private bool stopBulletMovement;
        public bool HasFired { get => hasFired; set => hasFired = value; }
        public Rigidbody2D Rb { get => rb; set => rb = value; }
        public Vector2 MovmentDirection { get => movmentDirection; set => movmentDirection = value; }
        public float MaxBulletLife { get => maxBulletLife; set => maxBulletLife = value; }
        public float BulletMovementSpeed { get => bulletMovementSpeed; set => bulletMovementSpeed = value; }
        public bool IsDisabled { get => isDisabled; set => isDisabled = value; }
        public bool IsNPCBullet { get => isNPCBullet; set => isNPCBullet = value; }
        public bool StopBulletMovement { get => stopBulletMovement; set => stopBulletMovement = value; }
        private PoolingManager poolingManager;
        private GameStateManager gameState;

        private void Awake()
        {
            Rb = GetComponent<Rigidbody2D>();
        }

        private void OnEnable()
        {
            IsDisabled = false;
            InitializeBullet();
        }
        public override void InitializeBullet()
        {
            gameState = GameManager.Instance.GameState;
            poolingManager = GameplayManager.Instance.poolingManager;

            HasFired = false;
            if (gameObject.layer == 6)
            {
                MaxBulletLife = GameManager.Instance.GameState.GameMetaData.PlayerMaxBulletLifeTime;
                BulletMovementSpeed = GameManager.Instance.GameState.GameMetaData.PlayerMaxBulletSpeed;
            }
            else if (gameObject.layer == 9)
            {
                MaxBulletLife = GameManager.Instance.GameState.GameMetaData.NPCMaxBulletLifeTime;
                BulletMovementSpeed = GameManager.Instance.GameState.GameMetaData.NPCMaxBulletSpeed;
            }

            MovmentDirection = transform.up;

            if (currentBulletType == BulletType.BigBoom)
            {
                StartCoroutine(StopBoomMovement());
            }
        }
        private void FixedUpdate()
        {
            if (HasFired && currentBulletType != BulletType.BigBoom)
            {
                transform.Translate(MovmentDirection * BulletMovementSpeed);
            }
            if (currentBulletType == BulletType.BigBoom && !StopBulletMovement)
            {
                transform.Translate(MovmentDirection * BulletMovementSpeed / 2);
                rb.AddTorque(1000.0f);
            }
            if (currentBulletType == BulletType.BirdFormation)
            {
                transform.Rotate(transform.up * 10.0f);
            }
        }
        private IEnumerator StopBoomMovement()
        {
            yield return new WaitForSecondsRealtime(0.5f);
            StopBulletMovement = true;
            ExplodeBoomBullet();
        }
        private void ExplodeBoomBullet()
        {
            GetComponent<SpriteRenderer>().enabled = false;
            GetComponent<BoxCollider2D>().enabled = false;

            for (int i = 0; i < 400;)
            {
                SpawnBoomBullet(i);
                i += 10;
            }
        }
        private void SpawnBoomBullet(float temp)
        {
            BulletBehaviour bulletBehaviour = poolingManager.ReturnPlayerBullet(BulletType.SingleFire);

            bulletBehaviour.BulletFired(Vector2.up, transform.position,
                Quaternion.RotateTowards(transform.rotation,
                Quaternion.Euler(transform.rotation.x, transform.rotation.y, transform.rotation.z + temp), temp),
                false);
        }
        public override void OnHit(Collision2D collision2D)
        {
            if (!IsDisabled)
                gameObject.SetActive(false);
        }
        private void OnCollisionEnter2D(Collision2D collision)
        {
            OnHit(collision);
        }
        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.transform.CompareTag("ScreenBoundries"))
            {
                transform.position = new Vector3((-1 * transform.position.x), (-1 * transform.position.y), transform.position.z);
            }
        }
        public override void BulletFired(Vector2 movementPoint, Vector3 bulletSpawnPoint, Quaternion bulletSpawnRotation, bool isNPCBullet)
        {
            gameObject.SetActive(true);
            transform.SetPositionAndRotation(bulletSpawnPoint, bulletSpawnRotation);
            HasFired = true;
            IsNPCBullet = isNPCBullet;
            MovmentDirection = movementPoint;
            StartCoroutine(DisableBullet());
        }
        private void OnDisable()
        {
            IsDisabled = true;
            GetComponent<SpriteRenderer>().enabled = true;
            GetComponent<BoxCollider2D>().enabled = true;
            if (!IsNPCBullet)
            {
                GameplayManager.Instance?.poolingManager.AddBullet(currentBulletType, gameObject);
            }
            else if (IsNPCBullet)
            {
                GameplayManager.Instance?.poolingManager.AddNPCBullet(gameObject);
            }
        }
        private IEnumerator DisableBullet()
        {
            yield return new WaitForSecondsRealtime(MaxBulletLife);
            if(!IsDisabled)
                gameObject.SetActive(false);
        }
    }
}