﻿using UnityEngine;

namespace manufactory.test.game.architecture
{
    public abstract class BulletBase : MonoBehaviour
    {
        public abstract void InitializeBullet();
        public abstract void BulletFired(Vector2 movementPoint, Vector3 bulletSpawnPoint, Quaternion bulletSpawnRotation, bool isNPCBullet);
        public abstract void OnHit(Collision2D collision2D);
        
    }
}

