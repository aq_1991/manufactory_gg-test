using manufactory.test.game.architecture.manager;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace manufactory.test.UI.views
{
    public class MainMenuView : MainUIView
    {
        public Button playBtn, leaderboardBtn;
        public override void Init()
        {
            Views = ViewType.MainMenu;
            base.Init();

            #region UI References

            playBtn.onClick.AddListener(PlayButtonClicked);
            leaderboardBtn.onClick.AddListener(LeaderboardButtonClicked);

            #endregion
        }

        private void PlayButtonClicked()
        {
            UIManager.ViewStatus(ViewType.MainMenu, false);
            UIManager.ViewStatus(ViewType.Loading, true);
            SceneManager.LoadScene(2);
        }
        private void LeaderboardButtonClicked()
        {
            UIManager.ViewStatus(ViewType.MainMenu, false);
            UIManager.ViewStatus(ViewType.Leaderboard, true);

        }
        public override void ShowPanel()
        {
            base.ShowPanel();
        }
        public override void HidePanel()
        {
            base.HidePanel();
        }
    }
}


