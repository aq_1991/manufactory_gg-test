using manufactory.test.game.architecture.manager;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace manufactory.test.UI.views
{
    public class InitializationView : MainUIView
    {
        public override void Init()
        {
            Views = ViewType.Initalization;
            base.Init();
            StartCoroutine(RedirectMainMenu());
        }
        public override void HidePanel()
        {
            base.HidePanel();
        }
        public override void ShowPanel()
        {
            base.ShowPanel();
        }
        IEnumerator RedirectMainMenu()
        {
            yield return new WaitForSecondsRealtime(1.5f);
            SceneManager.LoadScene(1);
        }
    }
}
