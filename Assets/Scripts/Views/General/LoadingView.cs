using manufactory.test.game.architecture.manager;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace manufactory.test.UI.views
{
    public class LoadingView : MainUIView
    {
        public override void Init()
        {
            Views = ViewType.Loading;
            base.Init();
        }
        public override void HidePanel()
        {
            base.HidePanel();
        }
        public override void ShowPanel()
        {
            base.ShowPanel();
        }
    }
}
