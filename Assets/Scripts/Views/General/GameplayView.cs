using manufactory.test.game.architecture.manager;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace manufactory.test.UI.views
{
    public class GameplayView : MainUIView
    {
        public TextMeshProUGUI playerLives, playerScore, CurrentLevel;
        private void OnEnable()
        {
            GameplayManager.OnGameScoresLivesUpdated += GameScoresLivesUpdated;
        }
        public override void Init()
        {
            Views = ViewType.Gameplay;
            base.Init();
        }
        private void GameScoresLivesUpdated(int scores, int lives, int currentLevel)
        {
            playerLives.text = $"Player Lives: {lives}";
            playerScore.text = $"Player Score: {scores}";
            CurrentLevel.text = $"Current level: {currentLevel + 1}";
        }
        public override void HidePanel()
        {
            base.HidePanel();
        }
        public override void ShowPanel()
        {
            base.ShowPanel();
        }
        private void OnDisable()
        {
            GameplayManager.OnGameScoresLivesUpdated -= GameScoresLivesUpdated;
        }
    }
}
