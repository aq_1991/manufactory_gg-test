using manufactory.test.game.architecture.manager;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace manufactory.test.UI.views
{
    public class GameFailedView : MainUIView
    {
        public TextMeshProUGUI totalScore;
        public Button restartBtn, mainMenuBtn;
        public override void Init()
        {
            Views = ViewType.GameFailed;
            InitializeUIReferences();
            base.Init();
        }
        private void InitializeUIReferences()
        {
            restartBtn?.onClick.AddListener(RestartGame);
            mainMenuBtn?.onClick.AddListener(BackMainMenu);
        }
        private void RestartGame()
        {
            UIManager.Instance.ViewStatus(ViewType.GameFailed, false);
            UIManager.Instance.ViewStatus(ViewType.Loading, true);
            SceneManager.LoadScene(2);
        }
        private void BackMainMenu()
        {
            UIManager.Instance.ViewStatus(ViewType.GameFailed, false);
            UIManager.Instance.ViewStatus(ViewType.Gameplay, false);
            UIManager.Instance.ViewStatus(ViewType.MainMenu, true);
            SceneManager.LoadScene(1);
        }
        public override void HidePanel()
        {
            base.HidePanel();
        }
        public override void ShowPanel()
        {
            totalScore.text = $"Your Score: {GameplayManager.Instance.CurrentPlayerScore}";
            base.ShowPanel();
        }
    }
}