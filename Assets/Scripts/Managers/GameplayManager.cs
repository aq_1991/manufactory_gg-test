using manufactory.test.game.architecture.behaviours;
using manufactory.test.game.architecture.data;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

namespace manufactory.test.game.architecture.manager
{
    public class GameplayManager : MonoBehaviour
    {
        public int PlayerLives = 3;
        public int CurrentPlayerScore = 0;
        public PlayerBehaviour playerRef;
        public Transform[] ObjectSpawnLocations;
        public PoolingManager poolingManager;
        public SpawnerManager spawnerManager;
        public GameStateManager gameState;
        public int CurrentLevel;
        public static GameplayManager Instance;

        public static Action<int, int, int> OnGameScoresLivesUpdated;

        private void Start()
        {
            if (Instance == null)
            {
                Instance = this;
                InitalizeGameplay();
            }
        }
        private void InitalizeGameplay()
        {
            UIManager.Instance.ViewStatus(ViewType.Loading, false);
            UIManager.Instance.ViewStatus(ViewType.Gameplay, true);

            PlayerLives = 3;
            CurrentLevel = 0;
            CurrentPlayerScore = 0;

            spawnerManager = new SpawnerManager();
            gameState = GameManager.Instance.GameState;
            UpdateScores(0);
        }
        public void UpdateScores(int score)
        {
            CurrentPlayerScore += score;
            if (CurrentPlayerScore >= gameState.GameMetaData.LevelScores[CurrentLevel])
            {
                CurrentPlayerScore += gameState.GameMetaData.LevelScores[CurrentLevel];
                CurrentLevel++;
            }
            OnGameScoresLivesUpdated?.Invoke(CurrentPlayerScore, PlayerLives, CurrentLevel);
        }
        public void UpdateLives()
        {
            PlayerLives--;
            OnGameScoresLivesUpdated?.Invoke(CurrentPlayerScore, PlayerLives, CurrentLevel);
            if (PlayerLives == 0)
            {
                UIManager.Instance.ViewStatus(ViewType.GameFailed, true);
            }
        }
        public async void RespawnPlayer()
        {
            playerRef.transform.SetPositionAndRotation(Vector3.zero, Quaternion.identity);
            playerRef.UpdateBulletType(BulletType.SingleFire);
            StartCoroutine(playerRef.StartImmortalTimer());
            await Task.Delay(500);
            playerRef?.gameObject.SetActive(true);
        }
        private void OnDisable()
        {
            Instance = null;
        }
    }
}
