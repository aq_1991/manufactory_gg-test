using manufactory.test.game.architecture.behaviours;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace manufactory.test.game.architecture.manager
{
    public class GameManager : Singleton<GameManager>
    {
        private GameStateManager gameState;
        private PlayerBehaviour playerBehaviour;

        public GameStateManager GameState { get => gameState; set => gameState = value; }
        public PlayerBehaviour PlayerBehaviour { get => playerBehaviour; set => playerBehaviour = value; }

        private void Start()
        {
            GameState = new GameStateManager();
            UIManager.Instance.ViewStatus(ViewType.Initalization, false);
            UIManager.Instance.ViewStatus(ViewType.MainMenu, true);
        }
    }
}


