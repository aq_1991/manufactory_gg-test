using manufactory.test.game.architecture.data;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace manufactory.test.game.architecture.manager
{
    public class GameStateManager
    {
        private GameMetaData gameMetaData;
        private PlayerMetaData playerMetaData;
        private UFOMetaData uFOMetaData;
        // Asteriods Metadata 
        private SmallAsteriodsMetaData smallAsteriodsMetaData;
        private MediumAsteriodsMetaData mediumAsteriodsMetaData;
        private LargeAsteriodsMetaData largeAsteriodsMetaData;
        // Power up Metadata
        private ArcFirePowerUp arcFire;
        private FireBallPowerUp fireBall;
        private ImmortalPowerUp immortalPower;
        private IncreaseFireRatePowerUp increaseFire;
        private SwarmingBirdPowerUp swarmingBird;

        public GameMetaData GameMetaData { get => gameMetaData; set => gameMetaData = value; }
        public PlayerMetaData PlayerMetaData { get => playerMetaData; set => playerMetaData = value; }
        public UFOMetaData UFOMetaData { get => uFOMetaData; set => uFOMetaData = value; }
        public LargeAsteriodsMetaData LargeAsteriodsMetaData { get => largeAsteriodsMetaData; set => largeAsteriodsMetaData = value; }
        public SmallAsteriodsMetaData SmallAsteriodsMetaData { get => smallAsteriodsMetaData; set => smallAsteriodsMetaData = value; }
        public MediumAsteriodsMetaData MediumAsteriodsMetaData { get => mediumAsteriodsMetaData; set => mediumAsteriodsMetaData = value; }
        public ArcFirePowerUp ArcFire { get => arcFire; set => arcFire = value; }
        public FireBallPowerUp FireBall { get => fireBall; set => fireBall = value; }
        public ImmortalPowerUp ImmortalPower { get => immortalPower; set => immortalPower = value; }
        public IncreaseFireRatePowerUp IncreaseFire { get => increaseFire; set => increaseFire = value; }
        public SwarmingBirdPowerUp SwarmingBird { get => swarmingBird; set => swarmingBird = value; }

        public GameStateManager()
        {
            GameMetaData = new GameMetaData();
            PlayerMetaData = new PlayerMetaData();
            UFOMetaData = new UFOMetaData();
            // Asteriods Metadata Init
            SmallAsteriodsMetaData = new SmallAsteriodsMetaData();
            MediumAsteriodsMetaData = new MediumAsteriodsMetaData();
            LargeAsteriodsMetaData = new LargeAsteriodsMetaData();
            // Power up Metadata Init
            ArcFire = new ArcFirePowerUp();
            FireBall = new FireBallPowerUp();
            ImmortalPower = new ImmortalPowerUp();
            IncreaseFire = new IncreaseFireRatePowerUp();
            SwarmingBird = new SwarmingBirdPowerUp();
        }


    }
}
