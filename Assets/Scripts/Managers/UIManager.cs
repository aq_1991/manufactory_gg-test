using Doozy.Engine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

namespace manufactory.test.game.architecture.manager
{
    public class UIManager : Singleton<UIManager>
    {
        [Header("General Game Views")]
        public List<MainUIView> gameUIViews = new List<MainUIView>();
        [Header("Pop ups")]
        public List<PopupUIView> genericUIPopup = new List<PopupUIView>();

        private void Start()
        {
            for (int i = 0; i < genericUIPopup.Count; i++)
            {
                genericUIPopup[i].Init();
            }

            for (int i = 0; i < gameUIViews.Count; i++)
            {
                gameUIViews[i].Init();
            }

        }

        #region UI Views

        public void ViewStatus(ViewType uIViews = ViewType.Loading, bool status = false)
        {
            MainUIView _gameObject = gameUIViews.Find(x => x.Views == uIViews);

            if (_gameObject == null)
            {
                Debug.LogError("You are trying to find a view which is not available in the list. Check the gameUIViews list: " + uIViews);
                return;
            }
            else
            {
                if (status)
                {
                    _gameObject.gameObject.SetActive(status);
                    _gameObject.ShowPanel();
                }
                else
                {
                    _gameObject.HidePanel();
                }
            }

        }
        public void ViewStatus()
        {
            for (int i = 0; i < gameUIViews.Count; i++)
            {
                gameUIViews[i].HidePanel();
            }
        }

        public GameObject ReturnMenuView(ViewType uIViews = ViewType.Loading)
        {
            return gameUIViews.Find(x => x.Views == uIViews).ViewObject;
        }
        #endregion

        #region Popup

        public void ViewGenericUIPopup(PopupViewType popupViews = PopupViewType.APILoader, bool status = false)
        {
            PopupUIView uIPopup = genericUIPopup.Find(x => x.PopViews == popupViews);

            if (uIPopup == null)
            {
                Debug.LogError("You are trying to find a view which is not available in the list. Check the genericUIPopup list: " + popupViews);
                return;
            }
            else
            {
                if (status)
                {
                    uIPopup.gameObject.SetActive(status);
                    uIPopup.ShowPanel();
                }
                else
                {
                    uIPopup.HidePanel();
                }
            }

        }
        public void ViewGenericUIPopup(PopupViewType popupViews = PopupViewType.APILoader, string heading = "Test Heading", string body = "Test Body", bool status = false)
        {
            PopupUIView uIPopup = genericUIPopup.Find(x => x.PopViews == popupViews);

            if (uIPopup == null)
            {
                Debug.LogError("You are trying to find a view which is not available in the list. Check the genericUIPopup list: " + popupViews);
                return;
            }
            else
            {
                if (string.IsNullOrEmpty(uIPopup.HeadingLabel) && string.IsNullOrEmpty(uIPopup.HeadingLabel))
                {
                    uIPopup.UpdatePopupLabels(heading, body);
                }


                if (status)
                {
                    uIPopup.gameObject.SetActive(status);
                    uIPopup.ShowPanel();
                }
                else
                {
                    uIPopup.HidePanel();
                }
            }
        }

        #endregion
    }

    #region UI Data Models
    [Serializable]
    public class MainUIView : MonoBehaviour
    {
        private ViewType views;
        private UIView viewParentObject;
        private GameObject viewObject;
        private string originalViewName;
        [HideInInspector]
        private UIManager uIManager;

        public UIManager UIManager { get => uIManager; set => uIManager = value; }
        public ViewType Views { get => views; set => views = value; }
        public UIView ViewParentObject { get => viewParentObject; set => viewParentObject = value; }
        public GameObject ViewObject { get => viewObject; set => viewObject = value; }

        public virtual void HidePanel()
        {
            if (ViewParentObject != null)
            {
                ViewParentObject.Hide();
                gameObject.name = originalViewName;
                gameObject.name += " - Hidden";
            }
            else
            {
                ViewObject.SetActive(false);
                gameObject.name = originalViewName;
                gameObject.name += " - Hidden";
            }
        }

        public virtual void Init()
        {
            UIManager = UIManager.Instance;
            ViewObject = gameObject;
            ViewParentObject = ViewObject.GetComponent<UIView>();   
            if (ViewParentObject != null)
            {
                originalViewName = ViewParentObject.gameObject.name;
                gameObject.name += " - Initalized";
            }
            else
            {
                originalViewName = ViewObject.name;
                ViewObject.name += " - Initalized";
            }
        }

        public virtual void ShowPanel()
        {
            if (ViewParentObject != null)
            {
                ViewParentObject?.Show();
                gameObject.name = originalViewName;
                gameObject.name += " - Showing";
            }
            else
            {
                ViewObject.SetActive(true);
                ViewObject.name = originalViewName;
                ViewObject.name += " - Showing";
            }

        }
    }
    [Serializable]
    public class PopupUIView : MonoBehaviour
    {
        public PopupViewType popViews;
        private UIPopup iPopup;
        private string headingLabel, messageBodyLabel, originalPopupName;
        private UIManager uIManager;
        public UIManager UIManager { get => uIManager; set => uIManager = value; }
        public UIPopup IPopup { get => iPopup; set => iPopup = value; }
        public PopupViewType PopViews { get => popViews; set => popViews = value; }
        public string HeadingLabel { get => headingLabel; set => headingLabel = value; }
        public string MessageBodyLabel { get => messageBodyLabel; set => messageBodyLabel = value; }
        public string OriginalPopupName { get => originalPopupName; set => originalPopupName = value; }

        public virtual void HidePanel()
        {
            iPopup.Hide();
            gameObject.name = originalPopupName;
            gameObject.name += " - Hidden";
        }

        public virtual void Init()
        {
            UIManager = UIManager.Instance;
            iPopup = GetComponent<UIPopup>();
            originalPopupName = gameObject.name;
            gameObject.name += " - Initalized";
        }

        public virtual void UpdatePopupLabels(string headingLabel = null, string messageBodyLabel = null)
        {
            this.headingLabel = headingLabel;
            this.messageBodyLabel = messageBodyLabel;
            iPopup.Data.SetLabelsTexts(headingLabel, messageBodyLabel);
        }

        public virtual void ShowPanel()
        {
            gameObject.name = originalPopupName;
            gameObject.name += " - Showing";
            iPopup?.Show(false);
        }
    }
    public enum ViewType
    {
        Initalization,
        UserAuthentication,
        MainMenu,
        Loading,
        Gameplay,
        Leaderboard,
        GameFailed
    }
    public enum PopupViewType
    {
        APILoader,
        GeneralErrorMessagePopup
    }
    #endregion
}