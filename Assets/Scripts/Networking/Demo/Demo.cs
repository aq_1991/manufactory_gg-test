using Newtonsoft.Json;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Demo : MonoBehaviour
{
    public RegisterUserSuccessResponse registerUserResponse = new RegisterUserSuccessResponse();
    public RegisterUserErrorResponse registerUserErrorResponse = new RegisterUserErrorResponse();
    public void CallAPI()
    {
        Networking.Instance.RegisterUser(null,null,null,
            OnSuccess =>
            {
                Debug.Log($"Success Server Callback: {OnSuccess}");
                registerUserResponse = JsonConvert.DeserializeObject<RegisterUserSuccessResponse>(OnSuccess);
            },
            OnFail =>
            {
                
                var registerUserError = JsonConvert.DeserializeObject<RegisterUserErrorResponse>(OnFail);
                if (!string.IsNullOrEmpty(registerUserError.error_code))
                {
                    Debug.LogError($"Failed Server Callback: {OnFail}");
                    registerUserErrorResponse = JsonConvert.DeserializeObject<RegisterUserErrorResponse>(OnFail);
                }
                else
                {
                    Debug.LogError($"FATAL ERROR: {OnFail}");
                }
            }
        );
    }
}
