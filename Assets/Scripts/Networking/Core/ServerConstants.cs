using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ServerConstants
{
    public const string HTTPEndPoint = null; //Paste the server IP here.

    public static bool UserLoggedIn = false;
    public static string userToken = null;
    public static string googleAuth = null;

    #region API URLs
    public const int RegisterActionId = 2000;
    #endregion
}
