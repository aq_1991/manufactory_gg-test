using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ServerResponses 
{

}

#region Server Models

#region Registeration Responses

public class RegisterUserSuccessResponse
{
    public string session_token;
    public int user_id;
    public bool success;
    public string user_name;
    public bool already_registered;

    public RegisterUserSuccessResponse()
    {
    }

    public RegisterUserSuccessResponse(RegisterUserSuccessResponse registerUserResponse)
    {
        session_token = registerUserResponse.session_token;
        user_id = registerUserResponse.user_id;
        success = registerUserResponse.success;
        user_name = registerUserResponse.user_name ?? throw new ArgumentNullException(nameof(user_name));
        already_registered = registerUserResponse.already_registered;
        ServerConstants.userToken = session_token;
    }

}

public class RegisterUserErrorResponse
{
    public string error_message;
    public bool success;
    public string error_code;

    public RegisterUserErrorResponse()
    {
    }

    public RegisterUserErrorResponse(RegisterUserErrorResponse registerUserErrorResponse)
    {
        error_message = registerUserErrorResponse?.error_message;
        success = registerUserErrorResponse.success;
        error_code = registerUserErrorResponse.error_code;
    }
}

#endregion



#endregion
