using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NetworkMesseges
{
    
}
#region Customized Classes for HTTP

public class GoogleLoginAuthentication
{
    public int action;
    public string tokenId;

    public GoogleLoginAuthentication(int action, string tokenId)
    {
        this.action = action;
        this.tokenId = tokenId ?? throw new ArgumentNullException(nameof(tokenId));
    }
}

public class RegisterNewUser
{
    public int action;
    public int app_id;
    public string api_key;
    public string user_name;
    public string password;
    public string email;

    public RegisterNewUser(int action, int app_id, string api_key, string user_name, string password, string email)
    {
        this.action = action;
        this.app_id = app_id;
        this.api_key = api_key ?? throw new ArgumentNullException(nameof(api_key));
        this.user_name = user_name ?? throw new ArgumentNullException(nameof(user_name));
        this.password = password ?? throw new ArgumentNullException(nameof(password));
        this.email = email ?? throw new ArgumentNullException(nameof(email));
    }
}




#endregion
