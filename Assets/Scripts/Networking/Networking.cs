﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;

public class Networking : MonoBehaviour
{
    private static Networking networking;
    [SerializeField]
    private bool isInternetConnected;

    public static Networking Instance 
    { 
        get => networking;
        set 
        {
            if (networking == null)
            {
                networking = value;
            }
        } 
    }

    public enum HTTP_REQUEST_TYPE
    {
        NONE,
        GET_REQUEST,
        POST_REQUEST
    }

    private void Start()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    #region API Builder Region
    public void GoogleSignIn(string tokenId, Action<string> OnSuccess = null, Action<string> OnFail = null)
    {
        //GoogleLoginAuthentication builder = new GoogleLoginAuthentication(tokenId, ServerConstants.RegisterActionId);
        //GeneralQuery(HTTP_REQUEST_TYPE.POST_REQUEST, ServerConstants.HTTPEndPoint, JsonConvert.SerializeObject(builder), OnSuccess, OnFail);
    }

    public void RegisterUser(string user_name = null, string password = null, string email = null, Action<string> OnSuccess = null, Action<string> OnFail = null)
    {
        RegisterNewUser builder = new RegisterNewUser(ServerConstants.RegisterActionId, 23, "sdasd", "ds33trtr", "123456789","sdas@ggds.com");
        GeneralQuery(HTTP_REQUEST_TYPE.POST_REQUEST, ServerConstants.HTTPEndPoint, JsonConvert.SerializeObject(builder), OnSuccess, OnFail);
    }


    #endregion

    #region General API Query
    private void GeneralQuery(HTTP_REQUEST_TYPE hTTP_REQUEST_TYPE, string API_Data,string jsonString, Action<string> OnAPISuccess = null, Action<string> OnAPIFailed = null)
    {
        if (InternetConnectionStatus())
        {
            Debug.Log($":::::::::: Sending Data ::::::::::\nHTTP ENUM REQUEST = {hTTP_REQUEST_TYPE}\nAPI URL = {API_Data}\nSending Json Object = {jsonString}");
        }
        else
        {
            Debug.LogError($"ERROR, NO INTERNET CONNECTION\n\n:::::::::: Sending Data ::::::::::\nHTTP ENUM REQUEST = {hTTP_REQUEST_TYPE}\nAPI URL = {API_Data}\nSending Json Object = {jsonString}");
            return;
        }
        switch (hTTP_REQUEST_TYPE)
        {
            case HTTP_REQUEST_TYPE.GET_REQUEST:

                StartCoroutine(HTTPGetMessage(API_Data,
                    OnSuccess =>
                    {
                        OnAPISuccess(OnSuccess);
                    },
                    OnFail =>
                    {
                        OnAPIFailed(OnFail);
                    }
                ));

                break;
            case HTTP_REQUEST_TYPE.POST_REQUEST:

                StartCoroutine(HTTPPostMessage(API_Data, jsonString,
                    OnSuccess =>
                    {
                        OnAPISuccess(OnSuccess);
                    },
                    OnFail =>
                    {
                        OnAPIFailed(OnFail);
                    }
                ));

                break;
        }
    }
    #endregion

    #region HTTP Protocol Messages
    public IEnumerator HTTPGetMessage(string API, Action<string> OnSuccess, Action<string> OnFail)
    {
        UnityWebRequest uwr = UnityWebRequest.Get(API);
        
        yield return uwr.SendWebRequest();

        RequestValidation(uwr, OnSuccess, OnFail);

    }
    public IEnumerator HTTPPostMessage(string API, string jsonToken, Action<string> OnSuccess = null, Action<string> OnFail = null)
    {
        var uwr = new UnityWebRequest(API, "POST");
        byte[] jsonToSend = new UTF8Encoding().GetBytes(jsonToken);
        uwr.uploadHandler = new UploadHandlerRaw(jsonToSend);
        uwr.downloadHandler = new DownloadHandlerBuffer();
        
        uwr.SetRequestHeader("Content-Type", "application/json");

        yield return uwr.SendWebRequest();

        RequestValidation(uwr, OnSuccess, OnFail);

    }
    #endregion

    #region General Functions
    public bool InternetConnectionStatus()
    {
        isInternetConnected = (Application.internetReachability == NetworkReachability.ReachableViaCarrierDataNetwork
            || Application.internetReachability == NetworkReachability.ReachableViaLocalAreaNetwork);
        return isInternetConnected;
    }

    private void RequestValidation(UnityWebRequest uwr, Action<string> OnSuccess = null, Action<string> OnFail = null)
    {
        bool _success = false;
        try
        {
            var responseObject = JsonConvert.DeserializeObject<Dictionary<string, object>>(uwr.downloadHandler.text);
            object temp;
            if (responseObject.TryGetValue("success", out temp))
            {
                _success = (bool)temp;
            }
        }
        catch (Exception ex)
        {
            Debug.Log(ex);
        }

        if (_success)
        {
            OnSuccess(uwr.downloadHandler.text);
        }
        else if (!_success)
        {
            OnFail(uwr.downloadHandler.text);
        }
        else if (!string.IsNullOrEmpty(uwr.error))
        {
            OnFail(uwr.error);
        }

    }

    #endregion
}
